﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Helper;
using Entities;
using Microsoft.Data.SqlClient;

namespace DataAccess.SqlQuary
{
    public static  class DeleteHelper
    {
        //PoldyCompany den silme yapıyor
        public static  void PoldyCompanyDeleteHelper(Companies company)
        {
           
            SqlConnection poldyCompanyCon = new SqlConnection("Data Source=DESKTOP-JFJJSJJ;Initial Catalog=PoldyCompany;Integrated Security=True"); poldyCompanyCon.Open();
            SqlCommand command = new SqlCommand("DELETE FROM Companies where CompanyId=" + company.CompanyId, poldyCompanyCon);
            command.ExecuteNonQuery();
            poldyCompanyCon.Close();

        }
        //AspNet Users Tablosundan Silme
        public static void UserDeleteHelper(Companies company)
        {
            SqlConnection aspUserConnection = new SqlConnection("Data Source=DESKTOP-JFJJSJJ;Initial Catalog=IdentityExample;Integrated Security=True");
            aspUserConnection.Open();
            SqlCommand com = new SqlCommand("DELETE FROM AspNetUsers where CompanyId=" + company.CompanyId, aspUserConnection);
            com.ExecuteNonQuery();
            aspUserConnection.Close();
        }

        //Şirket veritabanını siliyor
        public static  Boolean DropDatabaseCompany(Companies company)
        {
            var dropDatabase = DatabaseHelper.DropDatabase(company.CompanyName);

            if (dropDatabase)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
