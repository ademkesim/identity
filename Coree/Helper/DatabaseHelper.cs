﻿using Core.Utilities.Results;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace Core.Helper
{
    public static class DatabaseHelper
    {
        public static Boolean CreateCompanyTable(string databaseName)
        {
            string BaglantiAdresi = @"Server=DESKTOP-JFJJSJJ;initial catalog=" + databaseName + ";integrated security=true";
            SqlConnection con = new SqlConnection(BaglantiAdresi);
            string fileLocation = @"C:\Users\user\Desktop\PoldyBuKesinSon\a\SqlQuary\CreateCompany.txt";
            string quary = File.ReadAllText(fileLocation);
            quary = quary.Replace("DatabaseName", databaseName);
            SqlCommand myCommand = new SqlCommand(quary, con);
            try
            {
                con.Open();
                myCommand.ExecuteNonQuery();
                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }

            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public static Boolean CreateDatabase(string databaseName)
        {
            string BaglantiAdresi = @"Server=DESKTOP-JFJJSJJ;integrated security=true";
            SqlConnection con = new SqlConnection(BaglantiAdresi);
            string fileLocation = @"C:\Users\user\Desktop\PoldyBuKesinSon\a\SqlQuary\CreateDatabase.txt";
            string quary = File.ReadAllText(fileLocation);
            quary = quary.Replace("DatabaseName", databaseName);
            SqlCommand myCommand = new SqlCommand(quary, con);
            try
            {
                con.Open();
                myCommand.ExecuteNonQuery();
                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public static Boolean DropDatabase(string databaseName)
        {

            string BaglantiAdresi = @"Server=DESKTOP-JFJJSJJ;integrated security=true";
            SqlConnection con = new SqlConnection(BaglantiAdresi);
            string fileLocation = @"C:\Users\user\Desktop\PoldyBuKesinSon\a\SqlQuary\DropDatabase.txt";
            string quary = File.ReadAllText(fileLocation);
            quary = quary.Replace("DatabaseName", databaseName);
            SqlCommand myCommand = new SqlCommand(quary, con);
           
 
            string testConnection =
                @"Data Source=DESKTOP-JFJJSJJ;Initial Catalog="+databaseName+";Integrated Security=True";
            SqlConnection testcon=new SqlConnection(testConnection);
            try
            {
                con.Open();
                myCommand.ExecuteNonQuery();
                return true;
            }
            catch (Exception e)
            {
                try
                {
                    testcon.Open();
                    return false;
                }
                catch (Exception exception)
                {
                    return true;
                }
            }

        }


        //PoldyCompany den silme yapıyor
        public static void PoldyCompanyDeleteHelper(int companyId)
        {

            SqlConnection poldyCompanyCon = new SqlConnection("Data Source=DESKTOP-JFJJSJJ;Initial Catalog=PoldyCompany;Integrated Security=True"); poldyCompanyCon.Open();
            SqlCommand command = new SqlCommand("DELETE FROM Companies where CompanyId=" + companyId, poldyCompanyCon);
            command.ExecuteNonQuery();
            poldyCompanyCon.Close();

        }
        //AspNet Users Tablosundan Silme
        public static void UserDeleteHelper(int companyId)
        {
            SqlConnection aspUserConnection = new SqlConnection("Data Source=DESKTOP-JFJJSJJ;Initial Catalog=IdentityExample;Integrated Security=True");
            aspUserConnection.Open();
            SqlCommand com = new SqlCommand("DELETE FROM AspNetUsers where CompanyId=" + companyId, aspUserConnection);
            com.ExecuteNonQuery();
            aspUserConnection.Close();
        }

        //Şirket veritabanını siliyor
        public static Boolean DropDatabaseCompany(string companyName)
        {
            var dropDatabase = DatabaseHelper.DropDatabase(companyName);

            if (dropDatabase)
            {
                return true;
            }
            else
            {
                return false;
            }
        }




    }
}
