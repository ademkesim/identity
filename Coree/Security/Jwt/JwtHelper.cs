﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Core.Security.Encryption;
using Entities.Concrete;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Core.Security.Jwt
{
   public class JwtHelper:ITokenHelper
    {
        public IConfiguration Configuration;
        private TokenOptions _tokenOptions;
        private DateTime _accesTokenExpiration;
        private UserManager<User> UserMgr;


        public JwtHelper(IConfiguration configuration,UserManager<User> userManager)
        {
            Configuration = configuration;
            _tokenOptions = Configuration.GetSection("TokenOptions").Get<TokenOptions>();
            UserMgr = userManager;

        }
        public AccessToken CreateToken(User user)
        {
            _accesTokenExpiration = DateTime.Now.AddMinutes(_tokenOptions.AccessTokenExpiration);
            var securityKey = SecurityKeyHelper.CrSecurityKey(_tokenOptions.SecurityKey);
            var signingCredentials = SigningCredentialHelper.CreateSigningCredentials(securityKey);
            JwtSecurityToken jwt = CrJwtSecurityToken(_tokenOptions, signingCredentials,user);
            var JwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            var token = JwtSecurityTokenHandler.WriteToken(jwt);

            return new AccessToken()
            {
                Token = token,
                Expiration = _accesTokenExpiration
            };
        }

        public JwtSecurityToken CrJwtSecurityToken(TokenOptions tokenOptions, SigningCredentials signingCredentials,User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var claimUser = UserMgr.GetClaimsAsync(user).Result;
            claimUser.Add(new Claim("UserName", user.UserName));
            ClaimsIdentity claimsIdentity = new ClaimsIdentity();
            claimsIdentity.AddClaims(claimUser);
            var jwt = (JwtSecurityToken)
                tokenHandler.CreateJwtSecurityToken
                (
                    issuer: tokenOptions.Issuer,
                    audience: tokenOptions.Audience,
                    subject: claimsIdentity,
                    notBefore: DateTime.UtcNow,
                    expires: _accesTokenExpiration,
                    signingCredentials: signingCredentials
                );
            return jwt;
        }
    }
        
    }

