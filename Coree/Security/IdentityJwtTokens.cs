﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Security
{
    public class IdentityJwtTokens
    {
        public const string Issuer = "MVS";
        public const string Audience = "ApiUser";
        public const string Key = "1234567890123456";

        public const string AuthSchema = "Identity.Application" + "," + JwtBearerDefaults.AuthenticationScheme;
    }
}
