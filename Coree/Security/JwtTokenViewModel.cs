﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Security
{
    public class JwtTokenViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
