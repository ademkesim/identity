﻿using Business.Abstract;
using Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace IdentityAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private ICompanyService _companyService;
        private IPoldyCompanyService _poldyCompanyService;


        public CompanyController(ICompanyService companyService, IPoldyCompanyService poldyCompanyService)
        {
            _companyService = companyService;
            _poldyCompanyService = poldyCompanyService;
        }
        [Authorize(Policy = "IsPositionClaimAccess")]
        [HttpPost("add")]
        public IActionResult Add(Companies company)
        {
            var result = _companyService.Add(company);
            if (result.Success)
            {
               return Ok(result.Message);
            }
            return BadRequest(result.Message);
        }

        [Authorize(Policy = "CompanyUpdate")]
        [HttpPost("update")]
        public IActionResult Update(Companies company)
        {
            var result = _companyService.Update(company);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }


        [HttpPost("delete")]
        public IActionResult Delete(Companies company)
        {
            var result = _companyService.Delete(company);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
        [HttpGet("getlist")]
        public IActionResult GetList()
        {
            var result = _companyService.GetList();
            if (result.Success)
            {
                return Ok(result.Data);
            }

            return BadRequest(result.Message);
        }
        
    }
}
