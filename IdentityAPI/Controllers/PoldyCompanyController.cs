﻿using Business.Abstract;
using Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Constans;
using Entities.Concrete;

namespace IdentityAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PoldyCompanyController:ControllerBase
    {
        private IPoldyCompanyService _poldyCompanyService;

        public PoldyCompanyController(IPoldyCompanyService poldyCompanyService)
        {
            _poldyCompanyService = poldyCompanyService;
        }

        [Authorize(Policy = "IsPositionClaimAccess")]
        [HttpPost("add")]
        public IActionResult Add(Companies company)
        {
            var result = _poldyCompanyService.Add(company);
            if (result.Success)
            {
                return Ok(result.Message);
            }
            return BadRequest(result.Message);
        }
        [Authorize(Policy = "CompanyUpdate")]
        [HttpPost("update")]
        public IActionResult Update(Companies company)
        {
            var result = _poldyCompanyService.Update(company);
            if (result.Success)
            {
                return Ok(result.Message);
            }
            return BadRequest(result.Message);
        }
        [Authorize(Policy = "IsPositionClaimAccess")]
        [HttpGet("getUser")]
        public IActionResult GetUser(string userName)
        {
            var result = _poldyCompanyService.GetUser(userName);
            if (result.Success)
            {
                return Ok(result.Data);
            }
            return BadRequest("hata");
            
        }
        [Authorize(Policy = "IsPositionClaimAccess")]
        [HttpGet("getListMainCompany")]
        public IActionResult GetListMainCompany(int companyId)
        {
            var result = _poldyCompanyService.GetByMainCompany(companyId);
            if (result.Success)
            {
                return Ok(result.Data);
            }

            return BadRequest(result.Message);
        }
        [Authorize(Policy = "IsPositionClaimAccess")]
        [HttpGet("getListSubCompany")]
        public IActionResult GetListSubCompany(int subCompanyId)
        {
            var result = _poldyCompanyService.GetBySubCompany(subCompanyId);
            if (result.Success)
            {
                return Ok(result.Data);
            }

            return BadRequest(result.Message);
        }
        [Authorize(Policy = "IsPositionClaimAccess")]
        [HttpGet("getListCompany")]
        public IActionResult GetListCompany()
        {
            var result = _poldyCompanyService.GetList();
            if (result.Success)
            {
                return Ok(result.Data);
            }

            return BadRequest(result.Message);
        }
    }
}
