﻿using Business.Abstract;
using Entities.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace IdentityAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class AccountController : ControllerBase
    {
        private IAuthService _authService;
        public AccountController(IAuthService authService)
        {
            _authService = authService;
        }
        [HttpPut("logout")]
        public IActionResult Logout()
        {
            var result = _authService.UserExists();
            if (result.Success)
            {
               return Ok(result.Message);
            }
            return BadRequest(result.Message);
        }
        
        [HttpPost("register")]
        public IActionResult Register(UserForRegisterDto userForRegisterDto, string position)
        {
            var userExsits = _authService.UserExists();
            if (!userExsits.Success)
            {
                return BadRequest(userExsits.Message);
            }
            var result = _authService.Register(userForRegisterDto, position).Result;
            if (result.Success)
            {
                return Ok(result.Data);
            }
            return BadRequest(result.Message);
        }
        [HttpPost("login")]
        public IActionResult Login(UserForLoginDto userForLoginDto)
        {
            var userToLogin = _authService.Login(userForLoginDto);
            if (!userToLogin.Result.Success)
            {
                return BadRequest(userToLogin.Result.Message);
            }
            var user = userToLogin.Result.Data;
            var result = _authService.CreateAccessToken(user);
            if (result.Success)
            {
                return Ok(result.Data);
            }
            return BadRequest(result.Message);
        }
    }
}
