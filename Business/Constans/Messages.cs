﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Entities;

namespace Business.Constans
{
    public static class Messages
    {
        //User Messages
        public static string PreviouslyAdded = "Kullanıcı Zaten Eklenmiş";
        public static string ErrorRegister = "Kullanıcı Oluşturulamadı";
        public static string SuccesfulRegister = "Kullanıcı Başarıyla Oluşturuldu";
        public static string SuccesfulLogin = "Giriş Yapıldı";
        public static string UserNotFound = "Kullanıcı Bulunamadı";
        public static string PasswordError = "Hatalı Şifre Girildi";
        public static string SuccessExists = "Çıkış Yapıldı";
        public static string AccessTokenCreated = "Token Oluşturuldu";

        //Company Manager
        public static string CompanyAdded = "Şirket Eklendi";
        public static string ErrorCompanyAdded = "Şirket Eklenemedi";
        public static string CompanyDeleted = "Şirket Silindi";
        public static string ErrorCompanyDeleted = "Şirket Silinemedi";
        public static string CompanyUpdated = "Şirket Güncellendi";

        //Databasa Manager
        public static string CreateDatabase = "Veritabanı Oluşturuldu";
        public static string ErrorCreateDatabase = "Veritabanı Oluşturulamadı";
        public static string CreateCompanyTable = "Şirket Tablosu Oluşturuldu";
        public static string ErrorCreateCompanyTable = "Şirket Tablosu Oluşturulamadı";

        public static string ErrorGetMainCompany = "Ana Şireketler Listelenemedi";
        public static string ErrorGetSubCompany = "Alt Şireketler Listelenemedi";
        public static string SuccessGetMainCompany = "Ana Şireketler Listelendi";
        public static string SuccessGetSubCompany = "Alt Şireketler Listelendi";
        public static string ErrorCompanyList = "Şirketler Listelenemedi";
    }
}
