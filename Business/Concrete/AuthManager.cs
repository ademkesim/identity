﻿using Business.Abstract;
using Business.Constans;
using Core.Security.Jwt;
using Core.Utilities.Results;
using Entities.Concrete;
using Entities.Dtos;
using Microsoft.AspNetCore.Identity;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class AuthManager : IAuthService
    {
        private UserManager<User> UserMgr;
        private SignInManager<User> SignInMgr;
        private ITokenHelper _tokenHelper;
        public AuthManager(UserManager<User> userManager, SignInManager<User> signInManager, ITokenHelper tokenHelper)
        {
            UserMgr = userManager;
            SignInMgr = signInManager;
            _tokenHelper = tokenHelper;
        }

        public IDataResult<AccessToken> CreateAccessToken(User user)
        {
            var accessToken = _tokenHelper.CreateToken(user);
            return new SuccessDataResult<AccessToken>(accessToken, Messages.AccessTokenCreated);
        }

        public async Task<IDataResult<User>> Login(UserForLoginDto userForLoginDto)
        {
            var userToCheck = await UserMgr.FindByNameAsync(userForLoginDto.UserName);
            if (userToCheck == null)
            {
                return new ErrorDataResult<User>(Messages.UserNotFound);
            }
            var result = SignInMgr.PasswordSignInAsync(userForLoginDto.UserName, userForLoginDto.Password, false, false).Result;

            if (result.Succeeded)
            {
                return new SuccessDataResult<User>(userToCheck, Messages.SuccesfulLogin);
            }
            return new ErrorDataResult<User>(Messages.PasswordError);
        }

        public async Task<IDataResult<User>> Register(UserForRegisterDto userForRegisterDto, string position)
        {
            var userToCheck = UserMgr.FindByEmailAsync(userForRegisterDto.Email);
            if (userToCheck.Result != null)
            {
                return new ErrorDataResult<User>(Messages.PreviouslyAdded);
            }
            User user = new User
            {
                UserName = userForRegisterDto.UserName,
                Email = userForRegisterDto.Email,
                FirstName = userForRegisterDto.FirstName,
                LastName = userForRegisterDto.LastName,
                CompanyId = userForRegisterDto.CompanyId,
                SecurityStamp = Guid.NewGuid().ToString()
            };
            var result = UserMgr.CreateAsync(user, userForRegisterDto.Password);

            if (result.Result.Succeeded)
            {
                await AddClaim(userForRegisterDto.UserName, position);
                return new SuccessDataResult<User>(Messages.SuccesfulRegister);
            }
            return new ErrorDataResult<User>(Messages.ErrorRegister);
        }
        public async Task AddClaim(string userName, string position)
        {
            Claim claim = new Claim("position", position);
            User user = UserMgr.FindByNameAsync(userName).Result;
            await UserMgr.AddClaimAsync(user, claim);
        }

        public IResult UserExists()
        {
            SignInMgr.SignOutAsync();
            return new SuccessResult(Messages.SuccessExists);
        }

    }
}