﻿using System;
using Business.Abstract;
using Business.Constans;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Entities.Concrete;
using Microsoft.AspNetCore.Identity;

namespace Business.Concrete
{
    public class PoldyCompanyManager : IPoldyCompanyService
    {
        private IPoldyAdminDal _poldyAdminDal;
        private UserManager<User> UserMgr;
        private ICompanyDal _companyDal;

        public PoldyCompanyManager(IPoldyAdminDal poldyAdminDal, UserManager<User> userMgr, ICompanyDal companyDal)
        {
            _poldyAdminDal = poldyAdminDal;
            UserMgr = userMgr;
            _companyDal = companyDal;
        }
        public IResult Add(Companies company)
        {
            _poldyAdminDal.Add(company);
            return new SuccessResult(Messages.CompanyAdded);
        }

        public IResult Delete(Companies company)
        {
           _poldyAdminDal.Delete(company);
           return new SuccessResult(Messages.CompanyDeleted);
        }

        public IDataResult<List<Companies>> GetByMainCompany(int CompanyId)
        {
            
            var result=_poldyAdminDal.GetList(c => c.CompanyId == CompanyId);
            if (result!=null)
            {
                return new SuccessDataResult<List<Companies>>(result,Messages.SuccessGetMainCompany);
            }
            return new ErrorDataResult<List<Companies>>(Messages.ErrorGetMainCompany);
        }

        public IDataResult<List<Companies>> GetBySubCompany(int SubCompanyId)
        {
            var result = _poldyAdminDal.GetList(c => c.SubCompanyId==SubCompanyId);
            if (result != null)
            {
                return new SuccessDataResult<List<Companies>>(result, Messages.SuccessGetSubCompany);
            }
            return new ErrorDataResult<List<Companies>>(Messages.ErrorGetSubCompany);
        }

        public IDataResult<List<Companies>> GetList()
        {
            var result = _poldyAdminDal.GetList();
            if (result!=null)
            {
                return new SuccessDataResult<List<Companies>>(result);
            }
            return new ErrorDataResult<List<Companies>>(Messages.ErrorCompanyList);
        }
        
        public IResult Update(Companies company)
        {
          _poldyAdminDal.Update(company);
            return new SuccessResult(Messages.CompanyUpdated);
        }

        public IDataResult<GetUser> GetUser(string userName)
        {
            var result = UserMgr.FindByNameAsync(userName).Result;
            var company = _poldyAdminDal.Get(c => c.CompanyId == result.CompanyId);
            GetUser user = new GetUser(company.CompanyName,company.CompanyId,result.UserName,result.FirstName,result.LastName,result.Email,company.SubCompanyId,result.Id);
            return new SuccessDataResult<GetUser>(user);
        }
    }
}
