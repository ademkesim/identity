﻿using Business.Abstract;
using Business.Constans;
using Core.Helper;
using Core.Utilities.Results;
using DataAccess.Abstract;
using DataAccess.Concrete;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities;
using Entities.Concrete;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using DataAccess.SqlQuary;
using Microsoft.AspNetCore.Identity;

namespace Business.Concrete
{
    public class CompanyManager : ICompanyService
    {
        private ICompanyDal _companyDal;
        private IPoldyAdminDal _poldyAdminDal;
        private UserManager<User> UserMgr;


        public CompanyManager(ICompanyDal companyDal, IPoldyAdminDal poldyAdminDal, UserManager<User> userMgr)
        {
            _companyDal = companyDal;
            _poldyAdminDal = poldyAdminDal;
            UserMgr = userMgr;
        }
        public IResult Add(Companies company)
        {
            if (DatabaseHelper.CreateDatabase(company.CompanyName))
            {
                if (DatabaseHelper.CreateCompanyTable(company.CompanyName))
                {
                    string connectionString = "Data Source=DESKTOP-JFJJSJJ;Initial Catalog=" + company.CompanyName + ";Integrated Security=True";
                    FileUpdateHelper.AddOrUpdateAppSetting<string>("ConnectionStrings:CompanyData",connectionString);
                    _companyDal.Add(company);
                    return new SuccessResult(Messages.CompanyAdded);
                }
            }
            return new ErrorResult(Messages.ErrorCompanyAdded);
        }
        [Authorize()]
        public IResult Delete(Companies company)
        {

            /*  
              DeleteHelper.PoldyCompanyDeleteHelper(company);
              DeleteHelper.UserDeleteHelper(company);
              var dropDatabase=DeleteHelper.DropDatabaseCompany(company);
            */


            // Yukarıda comment edilmiş kod ile aşağıdaki kod bir birinin alternatifidir.


            DatabaseHelper.PoldyCompanyDeleteHelper(company.CompanyId);
            DatabaseHelper.UserDeleteHelper(company.CompanyId);
            var dropDatabase = DatabaseHelper.DropDatabase(company.CompanyName);
            var result = DeleteCompanyIdFromUser(company.CompanyId.ToString());

            if (result&&dropDatabase)
            {
                
                return new SuccessResult(Messages.CompanyDeleted);
            }
            return new ErrorResult(Messages.ErrorCompanyDeleted);

        }

        public bool DeleteCompanyIdFromUser(string id)
        {
            int comid = 0;
            bool isTrue = true;
            var user = UserMgr.Users;
            foreach (var x in user)
            {
                if (Convert.ToInt32(id) == x.CompanyId)
                {
                    comid = x.Id;
                    var res = UserMgr.DeleteAsync(x);

                    isTrue = true ? res.IsCompletedSuccessfully : false;

                }
            }

            return isTrue;
        }

        public IDataResult<List<Companies>> GetByMainCompany(int CompanyId)
        {
            return new SuccessDataResult<List<Companies>>(_companyDal.GetList(p => p.CompanyId == CompanyId));
        }

        public IDataResult<List<Companies>> GetBySubCompany(int SubCompanyId)
        {
            return new SuccessDataResult<List<Companies>>(_companyDal.GetList(p => p.SubCompanyId == SubCompanyId));
        }

        public IDataResult<List<Companies>> GetList()
        {
            return new SuccessDataResult<List<Companies>>(_companyDal.GetList());
        }

        

        public IResult Update(Companies company)
        {
            _companyDal.Update(company);
            return new SuccessResult(Messages.CompanyUpdated);
        }


    }
}
