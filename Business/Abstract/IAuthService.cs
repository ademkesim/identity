﻿using Core.Security.Jwt;
using Core.Utilities.Results;
using Entities.Concrete;
using Entities.Dtos;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IAuthService
    {
        Task<IDataResult<User>> Register(UserForRegisterDto userForRegisterDto, string position);
        Task<IDataResult<User>> Login(UserForLoginDto userForLoginDto);
        IResult UserExists();
        IDataResult<AccessToken> CreateAccessToken(User user);
    }
}
