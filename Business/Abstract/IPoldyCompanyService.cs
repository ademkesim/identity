﻿using Core.Utilities.Results;
using Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IPoldyCompanyService
    {
        IDataResult<List<Companies>> GetList();
        IDataResult<List<Companies>> GetByMainCompany(int CompanyId);
        IDataResult<List<Companies>> GetBySubCompany(int SubCompanyId);
        IResult Add(Companies company);
        IResult Delete(Companies company);
        IResult Update(Companies company);
        IDataResult<GetUser> GetUser(string userName);
        
    }
}
