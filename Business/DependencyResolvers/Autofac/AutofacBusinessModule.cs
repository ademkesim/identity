﻿
using Autofac;
using Business.Abstract;
using Business.Concrete;
using Core.Security.Jwt;
using System;
using System.Collections.Generic;
using System.Text;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework;
using Module = Autofac.Module;
using DataAccess.Concrete;

namespace Business.DependencyResolvers.Autofac
{
    public class AutofacBusinessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //Account
            builder.RegisterType<JwtHelper>().As<ITokenHelper>();
            builder.RegisterType<AuthManager>().As<IAuthService>();
            //Company
            builder.RegisterType<EfCompanyDal>().As<ICompanyDal>();
            builder.RegisterType<CompanyManager>().As<ICompanyService>();
            //PoldyAdmin
            builder.RegisterType<EfPoldyAdminDal>().As<IPoldyAdminDal>();
            builder.RegisterType<PoldyCompanyManager>().As<IPoldyCompanyService>();
        }
    }
}
