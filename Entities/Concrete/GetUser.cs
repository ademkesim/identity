﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Concrete
{
    public class GetUser
    {
        public string CompanyName { get; set; }
        public int CompanyId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int SubCompanyId { get; set; }
        public int UserId { get; set; }

        public GetUser(string companyName, int companyId, string userName, string firstName, string lastName, string email, int subCompanyId, int userId)
        {
            CompanyName = companyName;
            CompanyId = companyId;
            UserName = userName;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            SubCompanyId = subCompanyId;
            UserId = userId;
        }
    }
}
